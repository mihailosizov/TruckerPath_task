package com.google.maps;

import common.driver.Driver;
import common.utils.CommonUtils;
import io.appium.java_client.AppiumDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import java.util.*;

import static common.driver.AndriodVersions.*;
import static org.hamcrest.Matchers.*;
import static common.support.Assert.*;

public class AcceptanceTest {
    private AppiumDriver driver = Driver.getAndroidDriver(MARSHMALLOW600,
            GoogleMapsApp.ANDROID_6_0_APP_PATH,
            GoogleMapsApp.ANDROID_APP_PACKAGE,
            GoogleMapsApp.ANDROID_APP_ACTIVITY);
    private GoogleMapsApp testApp = new GoogleMapsApp(driver);

    private List<String> expectedFoodLinks = new ArrayList<>(Arrays.asList("Breakfast", "Lunch", "Coffee",
            "Dinner", "Drinks", "Best brunches", "Make it fast", "Where the locals go", "Bakeries", "Diners", "Best breakfasts",
            "Best spots for brunch with kids", "Make it cheap"));
    private final int expectedFoundLinksAmount = 6;

    private List<String> expectedPlaces = new ArrayList<>(Arrays.asList("Denny's", "Rally's", "Burger King", "McDonald's",
            "Dunkin' Donuts", "Baskin Robbins", "Papa John's Pizza", "T.J. Maloney’s Authentic Irish Pub"));
    private final int expectedFoundPlacesAmount = 2;

    private final int numberOfSwipesToCollectInfo = 4;
    private final String exploreFoodTestAddress = "3544 Van Buren St, Gary, IN 46408, USA";
    private final String searchBarDisappearTestAddress = "Antarctica";
    private final String directionsTestStartPoint = "Moscow, Russia";
    private final String directionsTestEndPoint = "New York, USA";

    private Set<String> exploreFoodElementsText = new HashSet<>();

    @Test(testName = "Explore Food Test: test links", priority = 1)
    public void exploreFoodLinksTest() {
        testApp.mainSearchScreen().performSearch(exploreFoodTestAddress);
        testApp.mainSearchScreen().selectFirstSearchResult();
        testApp.mainSearchScreen().getSearchItemInfo();
        testApp.searchItemInfo().exploreFoodAndDrinks();
        testApp.exploreFoodScreen().scrollFoodMenuHorizontalListToBeginning();

        testApp.exploreFoodScreen().selectBreakfastMenu();
        exploreFoodElementsText.addAll(testApp.exploreFoodScreen().collectTextOfAllTextElements(numberOfSwipesToCollectInfo));
        testApp.exploreFoodScreen().selectLunchMenu();
        exploreFoodElementsText.addAll(testApp.exploreFoodScreen().collectTextOfAllTextElements(numberOfSwipesToCollectInfo));
        testApp.exploreFoodScreen().selectCoffeeMenu();
        exploreFoodElementsText.addAll(testApp.exploreFoodScreen().collectTextOfAllTextElements(numberOfSwipesToCollectInfo));
        testApp.exploreFoodScreen().selectDinnerMenu();
        exploreFoodElementsText.addAll(testApp.exploreFoodScreen().collectTextOfAllTextElements(numberOfSwipesToCollectInfo));
        testApp.exploreFoodScreen().selectDrinksMenu();
        exploreFoodElementsText.addAll(testApp.exploreFoodScreen().collectTextOfAllTextElements(numberOfSwipesToCollectInfo));

        expectedFoodLinks.retainAll(exploreFoodElementsText);

        assertThat("More than " + expectedFoundLinksAmount + " expected links appeared on all food explore screens",
                expectedFoodLinks.size(), greaterThanOrEqualTo(expectedFoundLinksAmount));
    }

    @Test(testName = "Explore Food Test: test places", priority = 2)
    public void exploreFoodPlacesTest() {
        if (exploreFoodElementsText.isEmpty())
            throw new RuntimeException("There is no data for test");
        Set<String> actualPlaces = CommonUtils.parseCollection(exploreFoodElementsText, ",");
        expectedPlaces.retainAll(actualPlaces);
        assertThat("More than " + expectedFoundPlacesAmount + " expected food places appeared on all food explore screens",
                expectedPlaces.size(), greaterThanOrEqualTo(expectedFoundPlacesAmount));
    }

    @Test(testName = "Search Bar: disappear test", priority = 3)
    public void searchBarDisappearTest() {
        testApp.goToMainSearchScreen();
        testApp.mainSearchScreen().performSearch(searchBarDisappearTestAddress);
        testApp.mainSearchScreen().selectFirstSearchResult();
        boolean isDisplayedBeforeTap = testApp.mainSearchScreen().isSearchBarDisplayed();
        testApp.mainSearchScreen().tapTheMap();
        CommonUtils.standardWait();
        boolean isDisplayedAfterTap = testApp.mainSearchScreen().isSearchBarDisplayed();
        boolean isStateChangedOnce = isDisplayedBeforeTap != isDisplayedAfterTap;
        testApp.mainSearchScreen().tapTheMap();
        CommonUtils.standardWait();
        boolean isDisplayedAfterAnotherTap = testApp.mainSearchScreen().isSearchBarDisplayed();
        boolean isStateChangedTwice = isDisplayedAfterTap != isDisplayedAfterAnotherTap;
        assertThat("Search bar disappears after tap", isStateChangedOnce && isStateChangedTwice);
    }

    @Test(testName = "Directions: cannot calculate walking directions", priority = 4)
    public void directionsCannotCalculateTest() {
        testApp.goToMainSearchScreen();
        testApp.mainSearchScreen().openDirections();
        testApp.directionsScreen().setStartPoint(directionsTestStartPoint);
        testApp.directionsScreen().setEndPoint(directionsTestEndPoint);
        testApp.directionsScreen().chooseWalkingMode();
        assertThat("'No route found' alert is present", testApp.directionsScreen().isNoRouteAlertPresent());
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() {
        driver.quit();
    }
}
