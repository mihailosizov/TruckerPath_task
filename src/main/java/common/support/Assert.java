package common.support;

import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.slf4j.Logger;
import ru.yandex.qatools.allure.annotations.Step;

public class Assert {
    private static final Logger LOGGER = Log.getLogger(Assert.class);

    @Step
    public static void assertThat(String expected, boolean assertion) {
        if (!assertion) {
            LOGGER.error(String.format("[FAILED] Assertion failed: %s", expected));
            throw new AssertionError(expected);
        }
        LOGGER.info("[PASSED] Assertion passed. " + expected);
    }

    @Step
    public static <T> void assertThat(T actual, Matcher<? super T> matcher) {
        assertThat("", actual, matcher);
    }

    @Step
    public static <T> void assertThat(String expected, T actual, Matcher<? super T> matcher) {
        if (!matcher.matches(actual)) {
            StringDescription description = new StringDescription();
            description.appendText(expected).appendText(". Reason:\nExpected: ").appendDescriptionOf(matcher).appendText("\n     but: ");
            matcher.describeMismatch(actual, description);
            LOGGER.error(String.format("[FAILED] Assertion failed: %s", description.toString()));
            throw new AssertionError(description.toString());
        }
        LOGGER.info("[PASSED] Assertion passed. " + expected);
    }
}
