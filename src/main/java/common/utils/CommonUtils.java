package common.utils;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Parameter;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static common.driver.Driver.STANDARD_EXPL_TIMEOUT_MS;
import static common.driver.Driver.STANDARD_IMPL_TIMEOUT_SECONDS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class CommonUtils {
    private AppiumDriver driver;

    public CommonUtils(AppiumDriver driver) {
        this.driver = driver;
    }

    @Step
    public void swipeWholeScreen() {
        Dimension size = driver.manage().window().getSize();
        int startY = size.height - 1;
        int endY = (int) (size.height * 0.35);
        int staticX = 2;
        driver.swipe(staticX, startY, staticX, endY, 1000);
    }

    @Step
    public void swipeScreenUp() {
        Dimension size = driver.manage().window().getSize();
        int startY = (int) (size.height * 0.40);
        int endY = size.height - 1;
        int staticX = 2;
        for (int i = 0; i < 10; i++) {
            driver.swipe(staticX, startY, staticX, endY, 300);
        }
    }

    public Set<String> collectTextOfAllTextElements(int swipes, By textElementLocator) {
        swipeScreenUp();
        Set<String> result = new HashSet<>();
        for (int i = 0; i <= swipes; i++) {
            if (i > 0) swipeWholeScreen();
            List<WebElement> exploreFoodElements = driver.findElements(textElementLocator);
            for (WebElement element : exploreFoodElements) {
                result.add(element.getText());
            }
        }
        return result;
    }

    public Set<String> collectTextOfAllTextElements(By textElementLocator) {
        return collectTextOfAllTextElements(0, textElementLocator);
    }

    @Step
    public void waitForElementToDisappear(By elementLocator, String message) {
        List<WebElement> elementsToNotBeFound;
        boolean found = true;
        driver.manage().timeouts().implicitlyWait(500, MILLISECONDS);
        try {
            for (int i = 0; i < 100; i++) {
                MILLISECONDS.sleep(500);
                elementsToNotBeFound = driver.findElements(elementLocator);
                if (elementsToNotBeFound.size() == 0) {
                    found = false;
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            driver.manage().timeouts().implicitlyWait(STANDARD_IMPL_TIMEOUT_SECONDS, SECONDS);
        }
        if (found) throw new RuntimeException("Unable to " + message);
    }

    public void waitForProgress(By progressElementLocator) {
        waitForElementToDisappear(progressElementLocator, "wait until progress is finished");
    }

    @Step
    public void horizontalScrollToBeginning(WebElement scrollingElement) {
        int heigth = scrollingElement.getSize().getHeight();
        int width = scrollingElement.getSize().getWidth();
        int startX = scrollingElement.getLocation().getX();
        int startY = scrollingElement.getLocation().getY();
        for (int i = 0; i < 10; i++) {
            driver.swipe(startX + 1, startY + heigth / 2, width - 1, startY + heigth / 2, 300);
        }
    }

    public boolean isElementPresentAndDiplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static Set<String> parseCollection(Collection<String> inputCollection, String delimiter) {
        Set<String> result = new HashSet<>();
        for (String string : inputCollection) {
            String[] names = string.split(delimiter);
            for (String name : names) {
                result.add(name.trim());
            }
        }
        return result;
    }

    public static void standardWait() {
        try {
            TimeUnit.MILLISECONDS.sleep(STANDARD_EXPL_TIMEOUT_MS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
