package common.driver;

public enum AndriodVersions implements Version {
    MARSHMALLOW600("6.0.0"),
    MARSHMALLOW601("6.0.1"),
    LOLLIPOP50("5.0"),
    LOLLIPOP51("5.1.1");

    private String version;

    AndriodVersions(String version) {
        this.version = version;
    }

    public String toString() {
        return version;
    }
}
