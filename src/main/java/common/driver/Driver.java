package common.driver;

import common.support.Log;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import ru.yandex.qatools.allure.annotations.Step;

import java.net.MalformedURLException;
import java.net.URL;

import static io.appium.java_client.remote.MobileCapabilityType.*;
import static java.util.concurrent.TimeUnit.*;

public class Driver {
    public static final long STANDARD_IMPL_TIMEOUT_SECONDS = 10;
    public static final long STANDARD_EXPL_TIMEOUT_MS = 1000;
    private static final String APPIUM_SERVER_ADDRESS = "http://127.0.0.1:4723/wd/hub";
    private static final Logger LOGGER = Log.getLogger(Driver.class);

    private Driver() {
    }

    public static AndroidDriver getAndroidDriver(Version androidVersion, String testedAppPath, String appPackage, String appActivity) {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        capabilities.setCapability(DEVICE_NAME, "AndroidDriver");
        capabilities.setCapability(PLATFORM_VERSION, androidVersion.toString());
        capabilities.setCapability(PLATFORM_NAME, "Android");
        capabilities.setCapability(APP, testedAppPath);
        capabilities.setCapability(APP_PACKAGE, appPackage);
        capabilities.setCapability(APP_ACTIVITY, appActivity);
        return initializedAndroidDriver(capabilities);
    }

    @Step
    private static AndroidDriver initializedAndroidDriver(DesiredCapabilities capabilities) {
        LOGGER.info("Initializing android driver with the following capabilities: " + capabilities.asMap());
        AndroidDriver driver;
        try {
            driver = new AndroidDriver(new URL(APPIUM_SERVER_ADDRESS), capabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        driver.manage().timeouts().implicitlyWait(STANDARD_IMPL_TIMEOUT_SECONDS, SECONDS);
        return driver;
    }
}
