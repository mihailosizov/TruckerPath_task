package com.google.maps.screens;

import common.utils.CommonUtils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.Set;

abstract class AbstractScreen {
    protected AppiumDriver driver;
    protected CommonUtils commonUtils;
    protected By progressBarLocator = By.id("com.google.android.apps.maps:id/rover_progressbar");
    protected By textElementWidget = By.className("android.widget.TextView");

    protected AbstractScreen(AppiumDriver driver) {
        this.driver = driver;
        this.commonUtils = new CommonUtils(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void waitForProgress() {
        commonUtils.waitForProgress(progressBarLocator);
    }

    @Step
    public Set<String> collectTextOfAllTextElements(int swipes) {
        return commonUtils.collectTextOfAllTextElements(swipes, textElementWidget);
    }
}
