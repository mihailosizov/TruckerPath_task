package com.google.maps.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AndroidFindBys;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

public class DirectionsScreen extends MainSearchScreen {
    @AndroidFindBy(id = "com.google.android.apps.maps:id/directions_startpoint_textbox")
    private WebElement startPointBtn;

    @AndroidFindBy(id = "com.google.android.apps.maps:id/directions_endpoint_textbox")
    private WebElement endPointBtn;

    @AndroidFindBy(name = "Walking mode")
    private WebElement walkingModeBtn;

    @AndroidFindBy(id = TRIP_DETAILS_CONTAINER_ID)
    private WebElement tripDetailsContainer;

    @AndroidFindBys({
            @AndroidFindBy(id = TRIP_DETAILS_CONTAINER_ID),
            @AndroidFindBy(name = "No route found")
    })
    private WebElement noRouteFoundAlert;

    private static final String TRIP_DETAILS_CONTAINER_ID = "com.google.android.apps.maps:id/directions_trip_cardlist";

    public DirectionsScreen(AppiumDriver driver) {
        super(driver);
    }

    @Step
    public void setStartPoint(String startPoint) {
        startPointBtn.click();
        searchBar.sendKeys(startPoint);
        firstSearchResult.click();
    }

    @Step
    public void setEndPoint(String endPoint) {
        endPointBtn.click();
        searchBar.sendKeys(endPoint);
        firstSearchResult.click();
    }

    @Step
    public void chooseWalkingMode() {
        walkingModeBtn.click();
    }

    public boolean isNoRouteAlertPresent() {
        return commonUtils.isElementPresentAndDiplayed(noRouteFoundAlert);
    }
}
