package com.google.maps.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

public class SearchItemInfoScreen extends AbstractScreen {
    private static final String EXPLORE_FOOD_NAME = "Explore food & drinks here";

    @AndroidFindBy(name = EXPLORE_FOOD_NAME)
    private WebElement exploreFood;

    public SearchItemInfoScreen(AppiumDriver driver) {
        super(driver);
    }

    @Step
    public void exploreFoodAndDrinks() {
        exploreFood.click();
        waitForProgress();
    }
}
