package com.google.maps.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

public class MainSearchScreen extends AbstractScreen {
    @AndroidFindBy(id = "com.google.android.apps.maps:id/search_omnibox_text_box")
    protected WebElement searchBar;

    @AndroidFindBy(className = "android.widget.RelativeLayout")
    protected WebElement firstSearchResult;

    @AndroidFindBy(id = "com.google.android.apps.maps:id/header")
    private WebElement moreInfo;

    @AndroidFindBy(id = "com.google.android.apps.maps:id/mainmap_container")
    protected WebElement mapContainer;

    @AndroidFindBy(id = "com.google.android.apps.maps:id/on_map_directions_button")
    private WebElement directionsBtn;

    public MainSearchScreen(AppiumDriver driver) {
        super(driver);
    }

    @Step
    public void performSearch(String searchText) {
        searchBar.click();
        searchBar.sendKeys(searchText);
    }

    @Step
    public void selectFirstSearchResult() {
        firstSearchResult.click();
    }

    @Step
    public void getSearchItemInfo() {
        moreInfo.click();
    }

    @Step
    public void tapTheMap() {
        Dimension dimension = mapContainer.getSize();
        int x = (int) (dimension.getWidth() * 0.4);
        int y = (int) (dimension.getHeight() * 0.4);
        driver.tap(1, x, y, 50);
    }

    public boolean isSearchBarDisplayed() {
        return commonUtils.isElementPresentAndDiplayed(searchBar);
    }

    @Step
    public void openDirections() {
        directionsBtn.click();
    }
}
