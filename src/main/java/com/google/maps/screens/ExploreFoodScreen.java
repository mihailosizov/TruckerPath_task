package com.google.maps.screens;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.allure.annotations.Step;

public class ExploreFoodScreen extends SearchItemInfoScreen {
    @AndroidFindBy(className = "android.widget.HorizontalScrollView")
    WebElement foodMenuHorizontalList;

    @AndroidFindBy(name = "Breakfast")
    WebElement breakfastMenu;

    @AndroidFindBy(name = "Lunch")
    WebElement lunchMenu;

    @AndroidFindBy(xpath = "//android.widget.TextView[(@text='Coffee') or (@text='Coffee & Snacks')]")
    WebElement coffeeMenu;

    @AndroidFindBy(name = "Dinner")
    WebElement dinnerMenu;

    @AndroidFindBy(name = "Drinks")
    WebElement drinksMenu;

    public ExploreFoodScreen(AppiumDriver driver) {
        super(driver);
    }

    public void scrollFoodMenuHorizontalListToBeginning() {
        commonUtils.horizontalScrollToBeginning(foodMenuHorizontalList);
    }

    public void selectBreakfastMenu() {
        selectMenu(breakfastMenu);
    }

    public void selectLunchMenu() {
        selectMenu(lunchMenu);
    }

    public void selectCoffeeMenu() {
        selectMenu(coffeeMenu);
    }

    public void selectDinnerMenu() {
        selectMenu(dinnerMenu);
    }

    public void selectDrinksMenu() {
        selectMenu(drinksMenu);
    }

    @Step
    private void selectMenu(WebElement element) {
        element.click();
        waitForProgress();
    }
}
