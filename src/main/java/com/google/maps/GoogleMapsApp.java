package com.google.maps;

import com.google.maps.screens.DirectionsScreen;
import com.google.maps.screens.ExploreFoodScreen;
import com.google.maps.screens.MainSearchScreen;
import com.google.maps.screens.SearchItemInfoScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import ru.yandex.qatools.allure.annotations.Step;

public class GoogleMapsApp {
    public static final String ANDROID_6_0_APP_PATH = System.getProperty("user.dir") + "/testApps/google/android/Maps6.0.apk";
    public static final String ANDROID_APP_PACKAGE = "com.google.android.apps.maps";
    public static final String ANDROID_APP_ACTIVITY = "com.google.android.maps.MapsActivity";
    private AppiumDriver driver;

    public GoogleMapsApp(AppiumDriver driver) {
        this.driver = driver;
    }

    @Step
    public MainSearchScreen mainSearchScreen() {
        return new MainSearchScreen(driver);
    }

    @Step
    public SearchItemInfoScreen searchItemInfo() {
        return new SearchItemInfoScreen(driver);
    }

    @Step
    public ExploreFoodScreen exploreFoodScreen() {
        return new ExploreFoodScreen(driver);
    }

    @Step
    public DirectionsScreen directionsScreen() {
        return new DirectionsScreen(driver);
    }

    @Step
    public void goToMainSearchScreen() {
        //TODO: implement for iOS
        ((AndroidDriver) driver).startActivity(ANDROID_APP_PACKAGE, ANDROID_APP_ACTIVITY);
    }
}
